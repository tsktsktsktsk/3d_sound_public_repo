# -*- coding: utf-8 -*-
"""
Created on Mon Jan 02 20:48:25 2017

@author: ik-be
"""

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from mpl_toolkits.mplot3d import Axes3D #fails wsithout
import sounddevice as sd
import soundfile as sf
import sys #flush
import threading
import Queue as queue
import time
from matplotlib import gridspec
import math #FFT
from itertools import count



## For easier developing I have limited the amount of channels by hardcoding to 2. This can be changed in half a days work to support an arbitrary
## number of channels.




#add location properties to the sound object
#make eq plots per channel
#make input recognizable by ableton
 

 

       

        
        
#Audio properties:
PLAYBACK_AUDIO_FILE_NAME = "/Bent - As You Fall (Micah's Swollen Booty 360 Mix).wav"
SAVE_AUDIO_FILE_NAME = './3d_sound_output.wav'

BLOCKSIZE = 1200
SAMPLERATE = 48000.0
print sd.default.samplerate
#sd.default.samplerate = SAMPLERATE

#Sound object properties
AMOUNT_OF_DOTS = 1
DOT_R_LENGTH = 1
DOT_AZIMUTH_ANGLE = 90
DOT_INCLINATION_ANGLE = 0

ACTIVE_CHANNEL_KEYS = ['XL', 'XR']
ACTIVE_CHANNEL_COUNT = len(ACTIVE_CHANNEL_KEYS)

                
     
#Environment propertties
FLOOR = -1 #for plotting
CEILING = 1 #for plotting

CHANNEL_KEYS = ['XL', 'XR', 'YL', 'YR', 'ZL', 'ZR']
CHANNEL_COORDINATES =    [[ 1,0, 0],
                          [-1,0, 0],
                          [0, 1, 0],
                          [0,-1, 0],
                          [0, 0, 1],
                          [0, 0,-1]]
                          
CENTER = [0, 0, 0]

CHANNEL_COORDINATES_DICT = {channel_key: channel_coords for channel_key, channel_coords in zip(CHANNEL_KEYS, CHANNEL_COORDINATES)}
CHANNEL_COORDINATES_KEYSWITCH = {str(i[1]): i[0] for i in CHANNEL_COORDINATES_DICT.items()} #Where the coords are the keys

AMOUNT_OF_LINES = AMOUNT_OF_DOTS * len(CHANNEL_KEYS)

#PLOTTING PROPERTIES
AUDIO_ANIMATION_BOOLEANS = {'audio_plot_BOOL': True, #TODO: Fix the gridspec index assignment for the plot classes.
                           'fourier_plot_BOOL': True,
                           'spectro_plot_BOOL': False} #Makes the plotting very laggy        

start = time.time()
                                 


class Controller(object):
    
    
    def __init__(self, sound_environment):
        self.sound_environment = sound_environment
        self.env_sound = self.sound_environment.sound_obj
        self.env_dot_obj = self.sound_environment.sound_obj.DotObj            
        self.terminated_bool = False
        
    def onclick(self, event):
        #self.inclination = event.x
        print('button=%d, x=%d, y=%d, xdata=%f, ydata=%f' %
            (event.button, event.x, event.y, event.xdata, event.ydata))
    
    def on_key(self, event):
        #rewrite to function dict
        #RESERVED:  'o', 'l', 'k', 'f', 's'
        """
        if event.key == 'a':
            self.sound_environment.SoundDot.azimuth += 10
        if event.key == 'z':
            self.sound_environment.SoundDot.azimuth -= 10
        #""" 
        if event.key == 'a':
            self.env_sound.AlterSoundObj.modulo_filter += 1
        if event.key == 'z':
           self.env_sound.AlterSoundObj.modulo_filter -= 1
        if event.key == 'w':
            self.env_sound.AlterSoundObj.modulo_filter_bool = not self.env_sound.AlterSoundObj.modulo_filter_bool
        #if event.key == 's':
        #    self.sound_environment.SoundDot.inclination += 10
        if event.key == 'x':
            self.env_sound.play_file_bool = not self.env_sound.play_file_bool 
            print "PLAYING FILE: {} AT SAMPLERATE: {}".format(self.env_sound.play_file_bool, self.env_sound.f.samplerate)
        if event.key == 'd':
            self.env_dot_obj.sphere_r += 0.1
        if event.key == 'c':
            self.env_dot_obj.sphere_r -= 0.1
        if event.key == 'q':
            self.terminate_process()
        if event.key == 'y':
            self.env_dot_obj.inclination_speed_switch = not self.env_dot_obj.inclination_speed_switch
        if event.key == 'h':
            self.env_dot_obj.inclination_speed += 5
        if event.key == 'n':
            self.env_dot_obj.inclination_speed -= 5
        if event.key == 'u':
            self.env_dot_obj.azimuth_speed_switch = not self.env_dot_obj.azimuth_speed_switch
        if event.key == 'j':
            self.env_dot_obj.azimuth_speed += 1
        if event.key == 'm':
            self.env_dot_obj.azimuth_speed -= 1
            
        if event.key =='1':
            self.terminated_bool = True #Saves the animation
        print('you pressed', event.key, event.xdata, event.ydata)               
  
    def terminate_process(self):
        print "TERMINATING"
        self.env_sound.event.set()
        print "EVENT SET"
        self.env_sound.audio_stream.__exit__() #doesnt work ## ??
        print "AUDIO STREAM EXIT"
        self.__write_audio_file()
        print "closing plot"
        plt.close()
        
    def __write_audio_file(self):
        audio_stream = self.sound_environment.sound_obj.played_data_list
        samplerate = self.sound_environment.sound_obj.samplerate
        audio_stream = np.concatenate(audio_stream)
        sf.write(SAVE_AUDIO_FILE_NAME, audio_stream, int(samplerate))    
        print "WRITTEN FILE with samplerate: {}".format(int(samplerate))
        
        

class AnimatedPlot(object):
    
    
    def __init__(self, sound_environment, controller):
        self.sound_environment = sound_environment                             
        self.stream = self.sound_environment.DATA_STREAM()
        self.dotObj = self.sound_environment.sound_obj.DotObj
        
        self.controller = controller
        self.audio_plotter_obj = AudioPlotter(sound_environment)

        self.audio_animation_booleans = AUDIO_ANIMATION_BOOLEANS
        self.plot_object_names = ['environment_ax', 'scat'
                                  'audio_plot_L', 'audio_plot_R',
                                  'FFT_audio_line_L', 'FFT_audio_line_R',
                                  'spectro_gram_L_image', 'spectro_gram_R_image']

                            
        self.__set_up_gridspec_and_fig()
        self.__set_up_audio_feedback_plots()

        self.c = ['g', 'g', 'r', 'r', 'b', 'b']
        self.ani = animation.FuncAnimation(self.fig, self.update_animation, 
                                            interval= 16, #interval = ms delay between frames (60 hz = 16.6 144 hz = 7)
                                            init_func = self.setup_plot, blit = False)
        self.audio_plot_limit_size = BLOCKSIZE
                
    def __plot_wireframe(self): #not in use
        u, v = np.mgrid[0:2*np.pi:30j, 0:np.pi:30j]
        x=np.cos(u)*np.sin(v)
        y=np.sin(u)*np.sin(v)
        z=np.cos(v)
        self.environment_ax.plot_wireframe(x, y, z, color="r") 
        
    def __set_up_audio_feedback_plots(self):
        Y_LIM_TUPLE = (-4, 4)
        if self.audio_animation_booleans['audio_plot_BOOL']:
            self.audio_plot_L = self.fig.add_subplot(self.gs[1,0])
            self.audio_plot_R = self.fig.add_subplot(self.gs[1,1])            
            self.audio_plot_L.set_ylim(*Y_LIM_TUPLE)
            self.audio_plot_R.set_ylim(*Y_LIM_TUPLE)
            
        if self.audio_animation_booleans['fourier_plot_BOOL']:
            self.fourier_plot_L = self.fig.add_subplot(self.gs[2,0], xscale = 'log')#, yscale = 'log')
            self.fourier_plot_R = self.fig.add_subplot(self.gs[2,1], xscale = 'log')#,yscale = 'log') 
            self.fourier_plot_L.set_ylim(-1, 100)
            self.fourier_plot_R.set_ylim(-1, 100)
            
        if self.audio_animation_booleans['spectro_plot_BOOL']:
            self.spectro_gram_R_plot = self.fig.add_subplot(self.gs[3,1])
            self.spectro_gram_L_plot = self.fig.add_subplot(self.gs[3,0])         
 
    def __init_audio_plot_bools(self):
        if self.audio_animation_booleans['audio_plot_BOOL']:
            self.audio_line_L = self.audio_plot_L.plot(np.zeros(self.audio_plot_limit_size)) #Maybe not necessary?
            self.audio_line_R = self.audio_plot_R.plot(np.zeros(self.audio_plot_limit_size)) #Maybe not necessary?
            
        if self.audio_animation_booleans['fourier_plot_BOOL']:
            #NR_OF_FOURIER_BINS = 601
            self.FFT_audio_line_L = self.fourier_plot_L.plot(np.zeros(self.audio_plotter_obj.FFT_width)) #CHANGE the 600 (# of frequency bins)
            self.FFT_audio_line_R = self.fourier_plot_R.plot(np.zeros(self.audio_plotter_obj.FFT_width))
            
        if self.audio_animation_booleans['spectro_plot_BOOL']:
            #spect_shape = self.audio_plotter_obj.audio_plotdata_array
            self.spectro_gram_R_image = self.spectro_gram_R_plot.imshow(self.audio_plotter_obj.spectrogram_array_R, interpolation='none', animated=True) #
            self.spectro_gram_L_image = self.spectro_gram_L_plot.imshow(self.audio_plotter_obj.spectrogram_array_L, interpolation='none', animated=True) #
 
    def plot_audio(self):
        if self.audio_animation_booleans['audio_plot_BOOL']:
            self.audio_line_L[0].set_ydata(self.audio_plotter_obj.L)
            self.audio_line_R[0].set_ydata(self.audio_plotter_obj.R)
            
        if self.audio_animation_booleans['fourier_plot_BOOL']:
            self.FFT_audio_line_L[0].set_ydata((self.audio_plotter_obj.FFT_L_DATA))
            self.FFT_audio_line_R[0].set_ydata((self.audio_plotter_obj.FFT_R_DATA))
            
        if self.audio_animation_booleans['spectro_plot_BOOL']:
            self.spectro_gram_R_image.set_array(self.audio_plotter_obj.spectrogram_array_R)
            self.spectro_gram_L_image.set_array(self.audio_plotter_obj.spectrogram_array_L)
           
    def __set_up_gridspec_and_fig(self):
        self.fig = plt.figure()
        true_counter = self.audio_animation_booleans.values().count(True)
        self.gs = gridspec.GridSpec(1 + true_counter, 2) #0 = 3d plot, 1 = raw audio,  2 = fourier, 3 = spectro
        self.environment_ax = self.fig.add_subplot(self.gs[0,:], projection = '3d')
           
    def build_return_iterables_list(self):
        return [getattr(self, plot_object_name) for plot_object_name in self.plot_object_names if hasattr(self, plot_object_name)]      
            
    def setup_plot(self):
        self.__set_up_3d_plot()
        self.__init_audio_plot_bools()
        return_iterables = self.build_return_iterables_list()
        return return_iterables
              
    def __set_up_3d_plot(self):
        self.__init_sound_dot()
        self.__init_lines()
        self.__set_ax_properties()
        self.__setup_channel_dots()
        self.__setup_center_dot()
        self.__setup_ax_lines()
        
    def __init_sound_dot(self):
        next(self.stream)
        self.scat = self.environment_ax.scatter(*self.dotObj.cart_dot_coordinates,  s=300, animated=True)
        
    def __set_ax_properties(self):
        self.environment_ax.set_xlim3d(FLOOR, CEILING), self.environment_ax.set_ylim3d(FLOOR, CEILING), self.environment_ax.set_zlim3d(FLOOR, CEILING)
        self.environment_ax.set_aspect("equal")
        self.environment_ax.set_xlabel('X')
        self.environment_ax.set_ylabel('Y')
        self.environment_ax.set_zlabel('Z')

    def __init_lines(self):
        #3d plot
        self.environment_lines = [self.environment_ax.plot([0, 0], [0, 0], [0, 0], 
                                   color = self.c[i]) for i in range(AMOUNT_OF_LINES)] #line base

    def __setup_center_dot(self):
        self.environment_ax.scatter([0], [0], [0], color = 'r', s = 500) #Center dot
        
    def __setup_channel_dots(self):
        for i, channel_coord in enumerate(CHANNEL_COORDINATES): #channel dots
            self.environment_ax.scatter(*channel_coord, s = 100, color = self.c[i], label = str(i))
        
    def __setup_ax_lines(self):
        for i in range(0, 6, 2): #Three axes lines
            self.environment_ax.plot(*zip(*CHANNEL_COORDINATES[i:i+2]), linewidth=7.0, color = self.c[i])        
            
    def __update_lines(self, line_coords):
        for i in range(AMOUNT_OF_LINES): #Same order as edge_coords
            x, y, z = line_coords[i][0], line_coords[i][1], line_coords[i][2]
            self.environment_lines[i][0].set_data(x, y)
            self.environment_lines[i][0].set_3d_properties(z)
            
    def update_animation(self, i):
        next(self.stream)
        self.audio_plotter_obj.animation_update_loop()
        self.plot_audio()
        self.__update_lines(self.sound_environment.sound_obj.ChannelObj.line_coords_list)
        self.scat._offsets3d = (self.dotObj.cart_dot_coordinates)
        plt.draw()
        return_iterables = self.build_return_iterables_list()
        return return_iterables
        
    def show(self):
        self.fig.canvas.mpl_connect('button_press_event', self.controller.onclick)
        self.fig.canvas.mpl_connect('key_press_event', self.controller.on_key)
        plt.show()                                 
                           

        
class AudioPlotter(object):
    #This class is given the audio data and generates the data in the right format for plotting.
    
    def __init__(self, sound_environment):
        self.sound_environment = sound_environment
        self.audio_animation_booleans = AUDIO_ANIMATION_BOOLEANS
        self.L = 0 #Obviously not scalable with multiple channels in this way.
        self.R = 0 #Temp variables
        self.audio_data = []
        self.FFT_width = 2401


        self.raw_audio_array_R =  np.linspace(-1, 1, 1200) + np.linspace(-1, 1, 4).reshape(-1, 1)
        self.raw_audio_array_L =  np.linspace(-1, 1, 1200) + np.linspace(-1, 1, 4).reshape(-1, 1)
        
        self.spectrogram_array_L =  np.linspace(-1, 1, 601) + np.linspace(-1, 1, 200).reshape(-1, 1)
        self.spectrogram_array_R =  np.linspace(-1, 1, 601) + np.linspace(-1, 1, 200).reshape(-1, 1)
   
        
         #raw audio & fourier plots only atm.
    
    def __get_audio_data(self):
        q = self.sound_environment.sound_obj.q
        #print q.qsize()
        if q.qsize() > 2:
            while not q.empty():
                data = q.get_nowait()
                self.L, self.R = data[:,0], data[:,1]
                self.__update_raw_audio_array()
            
        self.data_for_FFT_R = np.concatenate(self.raw_audio_array_R, axis = 0)
        self.data_for_FFT_L = np.concatenate(self.raw_audio_array_L, axis = 0)
        self.audio_data = data #np.fromiter(chain(*audio_block), float)
        
    def __get_raw_audio_plot_data(self):
        self.L = self.audio_data[:, 0]
        self.R = self.audio_data[:, 1]
        
    def __update_spectrogram(self): #Not in use
        
        self.spectrogram_array_R = np.roll(self.spectrogram_array_R, 1, axis = 0)
        self.spectrogram_array_R[0, :] = self.FFT_R_DATA

        self.spectrogram_array_L = np.roll(self.spectrogram_array_L, 1, axis = 0)
        self.spectrogram_array_L[0, :] = self.FFT_L_DATA
    
    def __update_raw_audio_array(self):
        self.raw_audio_array_R = np.roll(self.raw_audio_array_R, 1, axis = 0)
        self.raw_audio_array_R[0, :] = self.R

        self.raw_audio_array_L = np.roll(self.raw_audio_array_L, 1, axis = 0)
        self.raw_audio_array_L[0, :] = self.L

        #_, _, self.Sxx = signal.spectrogram(self.raw_audio_array_R, SAMPLERATE)
        

        #self.audio_line_L[0].set_ydata(sound_data[:, 0])
        #self.audio_line_R[0].set_ydata(sound_data[:, 1])   
        
    def __get_fourier_plot_data(self):    
        #n = len(self.R)
        frequency_range = (20, 20000)
        nr_of_columns = 200
        gain = 10
        delta_f = (frequency_range[1] - frequency_range[0]) / nr_of_columns
        fftsize = math.ceil(SAMPLERATE / delta_f)
        
        Y_R = np.fft.rfft(self.data_for_FFT_R)#, n = int(fftsize))
        Y_R *= (gain / fftsize)
    
        Y_L = np.fft.rfft(self.data_for_FFT_L)#, n = int(fftsize))
        Y_L *= (gain / fftsize)
        print np.argmax((abs(Y_R))), np.shape(Y_R), fftsize, delta_f
        self.FFT_L_DATA = (abs(Y_L))
        self.FFT_R_DATA = (abs(Y_R))
    
    def animation_update_loop(self):
        self.__get_audio_data()
        self.__get_raw_audio_plot_data()
        self.__get_fourier_plot_data()
        #self.__update_spectrogram()
        
                
        
        
        
class Chanel(object):
    
    
    def __init__(self, dot_obj, origin_name):
        self.dot_obj = dot_obj
        self.origin_name = origin_name
        self.origin = CHANNEL_COORDINATES_DICT[origin_name]
        self.target_coords = dot_obj.cart_dot_coordinates_LIST #Starting position Dot Object (doesnt matter what the init value is)
        self.distance_to_dot = 0
        self.modulo_filter_counter = 0
        self.modulo_filter = 5
        self.distance_to_dot_2d_xyz_list = [0, 0, 0] #distance on the X, Y, Z plane (2d)
        #self.function_filter_pipeline = {100: self.__set_audio_amplitude_based_on_distance,
        #                                 110: self.__modulo_filter_func}

        self.channel_function_pipeline_dict = {'XL': {100: self.__set_audio_amplitude_based_on_distance,
                                                      110: self.__modulo_filter_func},
                                              'XR': {100: self.__set_audio_amplitude_based_on_distance}}

     
    def __modulo_filter_func(self, indata):
        channel_amplitude = 1.0
        if self.modulo_filter_counter % self.modulo_filter == 0:
            channel_amplitude = 0
            indata = indata * channel_amplitude
        self.modulo_filter_counter += 1  
        return indata
        
    def __set_distance_to_dot(self):
        xt, yt, zt = self.dot_obj.cart_dot_coordinates_LIST
        xo, yo, zo = self.origin
        self.distance_to_dot = ((xo - xt)**2 + (yo - yt)**2 + (zo - zt)**2)**0.5 / 2.0

    def __set_distance_to_dot_2d_xyz_list(self):
        self.distance_to_dot_2d_xyz_list = [(x0 - x1) for x0, x1 in zip(self.dot_obj.cart_dot_coordinates_LIST, self.origin)]
            
    def __set_audio_amplitude_based_on_distance(self, indata): 
        self.__set_distance_to_dot()
        self.__set_distance_to_dot_2d_xyz_list()
        indata = indata * self.distance_to_dot
        return indata
        
    def __set_phase_shift(self):
        pass
        
    def audio_update_loop(self, indata):
        sorted_keys = sorted(self.channel_function_pipeline_dict[self.origin_name].keys())
        for func_key in sorted_keys:
            indata = self.channel_function_pipeline_dict[self.origin_name][func_key](indata)
        return indata
    
    
    
class Chanels(object): #Lines


    def __init__(self, dot_obj):
        self.dot_obj = dot_obj
        self.channel_obj_dict = {channel_key: Chanel(dot_obj, channel_key) for channel_key in CHANNEL_KEYS}
        self.channel_key_index = {key: i for i, key in enumerate(ACTIVE_CHANNEL_KEYS)}


        self.target_coords = dot_obj.cart_dot_coordinates
        self.line_coords_list = []
        self.line_channel_length = {}
        self.channel_distance_dict = {}  
        self.function_filter_pipeline = {}
        self.function_keys_order = sorted(self.function_filter_pipeline.keys())

    def __get_line_coords_list(self):
        self.line_coords_list = []
        for channel_key in CHANNEL_KEYS:
            chanelObj = self.channel_obj_dict[channel_key]
            chanelObj.target = self.target_coords
            origin = chanelObj.origin
            line_coord = [[origin[i], self.target_coords[i][0]] for i in range(len(origin))] #This is ugly af - list & array
            self.line_coords_list.append(line_coord)  
            
    def __update_channels_seperate(self, indata):
        for channel_key in ACTIVE_CHANNEL_KEYS:
            channel_index = self.channel_key_index[channel_key]
            channel_indata = indata[:, channel_index]
            indata[:, channel_index] = self.channel_obj_dict[channel_key].audio_update_loop(channel_indata)
        return indata
            
    def __updata_channels_global(self, indata):
        for func_key in self.function_keys_order:
            indata = self.function_filter_pipeline[func_key](indata)
        return indata
        
    def audio_update_loop(self, indata):
        indata = self.__update_channels_seperate(indata)
        indata = self.__updata_channels_global(indata)
        return indata
  
    
    
    def animation_update_loop(self):
        self.target_coords = self.dot_obj.cart_dot_coordinates
        self.__get_line_coords_list()
           

        
        
        
class Sound_obj(object):
    
    def __init__(self):   
        self.buffersize = 100
        self.blocksize = BLOCKSIZE
        
        self.DotObj = Dot_obj()
        self.ChannelObj = Chanels(self.DotObj) 
        self.AlterSoundObj = Alter_sound(self.DotObj)
        
        self.event = threading.Event()
        #self.q = queue.LifoQueue()#Queue(maxsize=self.buffersize) #Always get the most recent data to plot.
        self.q = queue.Queue()
        self.f = sf.SoundFile(PLAYBACK_AUDIO_FILE_NAME).__enter__() if PLAYBACK_AUDIO_FILE_NAME is not None else None
        self.samplerate = self.f.samplerate if self.f is not None else SAMPLERATE
        
        print "SAMPLERATE : {}".format(self.samplerate)
        self.block_generator = self.f.blocks(blocksize = self.blocksize) if self.f is not None else None    
        self.audio_stream = sd.Stream(channels=ACTIVE_CHANNEL_COUNT,
                                      blocksize = self.blocksize,
                                      callback = self.callback,
                                      samplerate = self.samplerate,
                                      finished_callback = self.event.set)
        
        self.play_file_bool = False                          
        self.play_file_activated = False
        
        self.played_data_list = []
        
    def callback(self,indata, outdata, frames, time, status):
        if self.play_file_bool:
            indata =  next(self.block_generator) #Fix StopIteration when file is done buffering
            
        indata = self.AlterSoundObj.audio_update_loop(indata)
        indata = self.ChannelObj.audio_update_loop(indata)
        self.q.put_nowait(indata)
        self.played_data_list.append(indata.copy())
        outdata[:] = indata
            

           
class Alter_sound(object):
    #Class dedicated to altering the sound. Only the 'alter_sound' method is called which calles the
    #other functions in the order as depicted in the function_filter_pipeline
    #Affects the sound on ALL channels
    def __init__(self, dot_Obj):
        self.DotObj = dot_Obj
        self.modulo_filter = 1
        self.modulo_filter_counter = 0
        self.modulo_filter_bool = False
        self.channel_distance_dict = {key: 0 for key in ACTIVE_CHANNEL_KEYS} #Updated from Lines_obj
        self.function_filter_pipeline = {100: self.__modulo_filter_func,
                                         #105: self.__adjust_some_parts,
                                         110: self.__gen_sine_tone,
                                         200: self.__filler_func}
        self.function_keys_order = sorted(self.function_filter_pipeline.keys())
        self.sine_frequency = 300.0
        self.sine_gen = self.__sine_wave()
        

    def __sine_wave(self):
        period = int(SAMPLERATE / self.sine_frequency)
        lookup_table = [math.sin(2.0*math.pi*self.sine_frequency*(float(i%period)/SAMPLERATE)) for i in xrange(period)]
        return (lookup_table[i%period] for i in count(0))

    def __dynamic_sine_wave(self, frequency): #Not implemented
        self.sine_gen = self.__sine_wave(frequency = frequency)
        
    def __modulo_filter_func(self, indata):
        channel_amplitude = 1.0
        if self.modulo_filter_bool:
            if self.modulo_filter_counter % self.modulo_filter != 0:
                channel_amplitude = 0
                
        self.modulo_filter_counter += 1
        return indata * channel_amplitude
        
        
    def __adjust_some_parts(self, indata): #Remove certain tones
        skip_every_nr = 300
        for i in range(0, 100, 10):
            indata[0:len(indata):skip_every_nr - int(i*self.DotObj.cart_x)] *= abs(np.sin(i))
        return indata
        
    def __gen_sine_tone(self, indata): #Doesn't sound as smooth as it should.
        data_length = len(indata)
        x = np.array([next(self.sine_gen) for i in range(data_length)])
        for i in range(ACTIVE_CHANNEL_COUNT):
            indata[:,i] = indata[:,i] + self.__adjust_sine_tone_amp_by_z_axis_of_dot(x)
            #indata[:,i] = x
        #indata[:,1] = indata[:,1] #+ x
        return indata
        
    def __adjust_sine_tone_amp_by_z_axis_of_dot(self, sine_tone_data_array):
        return sine_tone_data_array * (self.DotObj.cart_z * 0.5)
    
        
    def __filler_func(self, indata):
        #indata[0:len(indata):4] = 0
        return indata 

    def audio_update_loop(self, indata):
        for function_key in self.function_keys_order:
           indata = self.function_filter_pipeline[function_key](indata)
        return indata    
        
    
        
class Environment(object):
    
    #azimuth, r and increment aren't capped to anything so when rotaion is selected it'll grow infinitely large - should be % 360 to save 'memory'.
    def __init__(self, sound_obj):
        self.sound_obj = sound_obj
        self.numlines = len(CHANNEL_COORDINATES) * AMOUNT_OF_DOTS
        self.line_coords_dict = {}
        self.new_scat_coords = np.zeros((1,3)) #Made for a single dot atm #TEMP TEMP TEMP TEMP
            
    def __update_function(self): #Controller update,  SoundDot update - Make LineObj dependend on DotObj
        self.sound_obj.DotObj.animation_update_loop()
        self.sound_obj.ChannelObj.animation_update_loop()

            
    def DATA_STREAM(self):
        #self.__set_new_scat_coords_spheric()
        with self.sound_obj.audio_stream:
            while True:    #Main loop
                self.__update_function()
                #sd.sleep(35) #sleep 35 milliseconds to save ram (?)
                yield True
           
 
        
class Dot_obj(object): #Merge Alter_sound
    

    def __init__(self):
        self.sphere_r = DOT_R_LENGTH
        self.inclination = DOT_INCLINATION_ANGLE
        self.azimuth = DOT_AZIMUTH_ANGLE
        
        self.cart_x_param = self.sphere_r
        self.cart_y_param = np.radians(self.azimuth)
        self.cart_z_param = np.radians(self.inclination)  
              
        self.sphere_dot_coordinates = (self.sphere_r, self.inclination, self.azimuth) 
        self.cart_dot_coordinates = [[0], [0], [0]] #x, y, z   
        self.cart_dot_coordinates_LIST = [0, 0, 0]

        self.start_sphere_rspeed = 3
        self.start_azimuth_speed = 1
        self.start_inclination_speed = 10
        
        self.azimuth_speed = 0
        self.inclination_speed = 0
        self.sphere_r_speed = 0
        
        self.sphere_r_speed_switch = False
        self.azimuth_speed_switch = False
        self.inclination_speed_switch = False      
 
        
    def __cart_to_spheric(self):
        self.sphere_r = np.sqrt(self.cart_x**2 + self.cart_y**2 + self.cart_z**2)
        self.azimuth = np.arccos(self.cart_z / self.sphere_r)
        self.inclination = np.arctan(self.cart_y / self.cart_x)
        
    def __spheric_to_cart(self):
        self.cart_x_param = self.sphere_r #obsolete
        self.cart_y_param = np.radians(self.azimuth) #Shouldn't be necessary
        self.cart_z_param = np.radians(self.inclination)
        
        self.cart_x = self.sphere_r * np.sin(self.cart_y_param) * np.cos(self.cart_z_param)
        self.cart_y = self.sphere_r * np.sin(self.cart_y_param) * np.sin(self.cart_z_param)
        self.cart_z = self.sphere_r * np.cos(self.cart_y_param) 
        self.cart_dot_coordinates = ([self.cart_x], [self.cart_y], [self.cart_z])
        self.cart_dot_coordinates_LIST = [self.cart_x, self.cart_y, self.cart_z]
        
    def __speed_switches(self): #move to Controller
        if self.azimuth_speed_switch:
            self.azimuth += self.start_azimuth_speed + self.azimuth_speed 
        if self.inclination_speed_switch:
            self.inclination += self.start_inclination_speed + self.inclination_speed
        if self.sphere_r_speed_switch:
           self.sphere_r += self.start_sphere_r_speed + self.sphere_r_speed      

    def animation_update_loop(self):
        self.__speed_switches()
        self.__spheric_to_cart() #its controlled in spherical coordinates but plotted in cartesian
        
        
       


sound_obj = Sound_obj()
environment = Environment(sound_obj)
controller = Controller(environment)
animated_plot = AnimatedPlot(environment, controller)
animated_plot.show()


#ml = main_loop(Environment, Controller, Sound_obj, AnimatedPlot)
#ml.run_main_loop()   
        
